package edu.ucsd.cs110w.temperature;

/** Class Celsius
 * Models a temperature class using the units of Celsius
 *
 * @author mvkrishn
 */
public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t); 
	}
	public String toString() {
		return this.getValue() + " C"; 
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) 9/5*this.getValue() + 32);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin(this.getValue() + 273.15f);
	}
}
