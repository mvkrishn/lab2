package edu.ucsd.cs110w.temperature;

/** Class Fahrenheit
 * Models a temperature class using the units of Fahrenheit
 *
 * @author mvkrishn
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t); 
	}
	public String toString() {
		return this.getValue() + " F"; 
	}
	
	@Override
	public Temperature toCelsius() {
		return new Celsius((float) 5/9*(this.getValue() - 32));
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin((float) 5/9*(this.getValue() - 32) + 273.15f);
	}

}
