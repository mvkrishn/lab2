/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/** Class Celsius
 * Models a temperature class using the units of Celsius
 *
 * @author mvkrishn
 */
public class Kelvin extends Temperature{

	public Kelvin(float t) 
	{ 
		super(t); 
	} 

	public String toString() 
	{ 
		return this.getValue() + " K"; 
	} 
	
	@Override 
	public Temperature toCelsius() { 
		return new Celsius((float) this.getValue() - 273.15f); 
	} 
	@Override 
	public Temperature toFahrenheit() { 
		return new Fahrenheit((float) 9/5*(this.getValue() - 273.15f) + 32); 
	}

	@Override
	public Temperature toKelvin() {
		return this;
	}


}
